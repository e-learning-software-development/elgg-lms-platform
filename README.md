# Elgg LMS Platform

Software is provided under an AS-IS basis and without any support, updates or maintenance. 
Nothing in this Agreement shall require Licensor to provide Licensee with support or fixes to any bug, 
failure, mis-performance or other defect in The Software.

## Getting Started

Elgg LMS Platform runs on a combination of the Apache web server, MySQL database 
system and the PHP interpreted scripting language. This is the most 
popular web server environment in the world. (Elgg can also run on
other web servers such a nginx and IIS, but requires further configuration).

### Prerequisites

Due to Elgg's advanced functionality, there are some extra 
configuration requirements

* Apache web server 
* mod_rewrite enabled
* PHP 5.6
* MySQL 5+.
* GD Library(for graphics processing such as avatar cropping)
* Multibyte String support (for internationalization)

### Server configuration

It is recommended that you increase the memory available to PHP 
threads beyond the standard 8 or 12M, and increase the maximum 
uploaded filesize (which defaults to 2M). In both cases, this can be
found in your php.ini.

## Installation

Before you begin, make sure you have read Elgg's technical 
requirements.

* Clone this repository in your server: https://gitlab.com/e-learning-software-development/elgg-lms-platform.git
* Create a data folder

Essential Information about the data folder                                |  
---------------------------------------------------------------------- | 
Elgg needs a special folder to store uploaded files, including profile icons and photos. You will need to create this for it. |  
We recommend that this folder is called data, and is stored outside of your document root. For example, if Elgg is installed in /home/elgg/html/, you might create it in /home/elgg/data.              | 
Make sure that your web server has permission to write to it. |

* Create a database

* Start the installer by visiting the URL of the site and follow the instructions

### Notes about the Installer

The Installer will try and create two files for you:

    * engine/settings.php, which contains the database settings for your installation
    * .htaccess, which allows Elgg LMS Platform to generate dynamic URLs 

If your web server does not have permission to create these files, you
will need to either 

* Change the permissions on the directory where you are installing Elgg and the engine directory and try again. Remember to change the 
permissions back to the original values after installation is complete.

* Copy engine/settings.example.php to engine/settings.php, open it up in a text editor and fill in your database details. Then copy /htaccess_dist to /.htaccess 


## Deployment

You just installed the bare Elgg LMS platform. Make sure the following plugins are activated:
* User Dashboard
* File plugin
* Messages
* Notifications
* Profile
* Profile Manager
* Roles
* Roles for Profile Manager
* User Import
* ZAudio
* Embed plugin


### Optionals plugins (Depending of your needs):
* Advanced Statistics
* Bookmarks
* Site-wide Categories
* Event Calendar
* Site Pages
* Login As
* Message Board
* Members
* Minify (for image optimization)
* Pages
* Polls
* Search
* SNA4Elgg


We are still missing some key plugins, which are in separate repositories. The reason for this is to have
a better development management. Here's the list of plugins you must clone/download into the mod/ folder (and activate it in this order):

* [Groups Plugin](https://gitlab.com/pigo-lms-developemt/groups)
* [Group Tools](https://gitlab.com/pigo-lms-developemt/group_tools)
* [TinyMce Pro](https://gitlab.com/pigo-lms-developemt/tinymce_pro)
* [Izap Elgg Bridge](https://gitlab.com/pigo-lms-developemt/izap-elgg-bridge)
* [Izap Contest](https://gitlab.com/pigo-lms-developemt/izap-contest)
* [Roles Plugin](https://gitlab.com/pigo-lms-developemt/roles)
* [Roles Moderators](https://gitlab.com/pigo-lms-developemt/roles_moderators)
* [Students Role](https://gitlab.com/pigo-lms-developemt/students)
* [WebODF for elgg](https://gitlab.com/pigo-lms-developemt/webodf_elgg)
* [Gentelella Custom Theme for Elgg](https://gitlab.com/pigo-lms-developemt/gentelella-custom)


## Built With

* [Elgg](https://github.com/Elgg/Elgg) - Elgg is an open source rapid development framework for socially aware web applications.
* [Gentelella Theme](https://github.com/ColorlibHQ/gentelella) - The Bootstrap admin template used
* [Office to HTML](https://github.com/meshesha/officeToHtml) - jQuery plugin for previewing MS Office and pdf files
* [ViewerJS](https://github.com/webodf/ViewerJS) - View presentations, spreadsheets, PDF's and other documents on your website
* [WebODF](https://github.com/webodf/WebODF) - WebODF: work with your office files in the cloud, on the desktop and on your mobile
* [Icons made by Freepik from Flaticon](https://www.flaticon.com/authors/freepik) - Flaticon.com 

## License

Copyright (c) 2012-2019

Elgg is released under the GNU General Public License (GPL) Version 2